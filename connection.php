<?php
/**
 * Created by PhpStorm.
 * User: Patrick
 * Date: 23-3-2016
 * Time: 10:07
 */

define('hostname','localhost');
define('user','root');
define('password','');
define('database','android_db');

$connect = new mysqli(hostname,user,password,database);

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}