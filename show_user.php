<?php
/**
 * Created by PhpStorm.
 * User: Patrick
 * Date: 23-3-2016
 * Time: 10:12
 */

if($_SERVER['REQUEST_METHOD']) {
    require 'connection.php';
    showUsers();
}

function showUsers() {

    global $connect;

    $query = "SELECT * FROM `user` ORDER BY `create_time` DESC";
    if($result = $connect->query($query)) {
        $users = array();

        while($row = $result->fetch_assoc()) {
            $users[] = $row;
        }

        header('Content-type:appplication/json');
        echo json_encode(array("users" => $users , 'count' => $result->num_rows )  );


    }else {
        echo $connect->error;
    }

    $connect->close();

}